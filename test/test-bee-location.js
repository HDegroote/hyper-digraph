const { expect } = require('chai')

const { hyperInterfaceFactory } = require('./fixtures')
const BeeLocation = require('../lib/bee-location')
const utils = require('../lib/utils')
const c = require('../lib/constants')

describe('Test bee-location', function () {
  const location = 'loc'
  const hash = 'a'.repeat(64)

  describe('Returns the expected uniqueId', function () {
    it('Returns the correct unique id if no version specified', function () {
      const e = new BeeLocation({ hash, location })
      expect(e.uniqueId).to.equal(`${hash}locundefined`)
    })

    it('Returns the correct unique id if version specified', function () {
      const e = new BeeLocation({ hash, location, version: 2 })
      expect(e.uniqueId).to.equal(`${hash}loc2`)
    })
  })

  it('Can create a valid bee-location', function () {
    const e = new BeeLocation({ hash, location })
    expect(e.hash).to.equal(hash)
    expect(e.location).to.equal(location)
    expect(e.version).to.equal(undefined)
  })

  it('Can create a valid versioned bee-location', function () {
    const e = new BeeLocation({ hash, location, version: 5 })
    expect(e.hash).to.equal(hash)
    expect(e.location).to.equal(location)
    expect(e.version).to.equal(5)
  })

  describe('Test invalid beeLocations throw', function () {
    const invalids = [
      [{ hash, location: '' }, 'empty location'],
      [{ hash: 'a'.repeat(63), location }, 'hash too short'],
      [{ hash, location, version: 'Nope' }, 'version no int'],
      [{ hash }, 'No location'],
      [{ location }, 'No hash']

    ]

    invalids.forEach(([beeLocation, desc]) => {
      it(`throws on invalid entry req (${desc})`, function () {
        expect(() => new BeeLocation(beeLocation)).to.throw()
      })
    })
  })

  it('Has the correct toStr when no version specified', function () {
    const beeLocation = new BeeLocation({ hash, location })
    expect(beeLocation.toStr()).to.equal(`${hash}/loc[latest]`)
  })

  it('Has the correct toStr when version specified', function () {
    const beeLocation = new BeeLocation({ hash, location, version: 1 })
    expect(beeLocation.toStr()).to.equal(`${hash}/loc[1]`)
  })

  describe('Test getting the entry', function () {
    let hyperInterface
    let hyperbee, beeLocation
    const entry = 'I am an entry'
    const loc = 'someLoc'
    let getEntryFunc

    this.beforeEach(async function () {
      hyperInterface = await hyperInterfaceFactory()

      hyperbee = await hyperInterface.createHyperbee('testbee')
      await hyperbee.put(loc, entry)

      beeLocation = new BeeLocation(
        { hash: hyperbee.feed.key, location: loc }
      )

      getEntryFunc = async (entry) => await hyperInterface.getEntry(entry)
    })

    this.afterEach(async function () {
      await hyperInterface.close()
    })

    it('Can get the entry', async function () {
      expect((await beeLocation.resolve(getEntryFunc))).to.equal('I am an entry')
    })

    it('Can get an old entry version', async function () {
      const version = hyperbee.version

      const newEntry = 'new entry'
      await hyperbee.put(loc, newEntry)

      const oldBeeLocation = new BeeLocation(
        { hash: hyperbee.feed.key, location: loc, version }
      )
      expect((await oldBeeLocation.resolve(getEntryFunc))).to.equal(entry)

      // Sanity check
      expect((await beeLocation.resolve(getEntryFunc))).to.equal('new entry')
    })

    it('Does not include the version if undefined for toJson', function () {
      expect(beeLocation.toJson()).to.deep.equal(
        { [c.HASH]: utils.getHashAsStr(hyperbee.feed.key), [c.LOCATION]: loc }
      )
    })

    it('Does include the version if defined for toJson', function () {
      beeLocation = new BeeLocation({ ...beeLocation, version: 1 })
      expect(beeLocation.toJson()).to.deep.equal(
        {
          [c.HASH]: utils.getHashAsStr(hyperbee.feed.key),
          [c.LOCATION]: loc,
          [c.VERSION]: 1,
          [c.VERSION]: 1
        }
      )
    })
  })
})
