const { expect } = require('chai')
const { strict: nodeAssert } = require('assert')
const sinon = require('sinon')

const { hyperInterfaceFactory } = require('./fixtures')
const c = require('../lib/constants')
const Node = require('../lib/node')
const BeeLocation = require('../lib/bee-location')
const BeeInterface = require('../lib/bee-interface')

describe('Test Node', function () {
  const location = 'loc'
  let hyperInterface, hyperbee
  let beeLocation, entry, node
  let backend

  this.beforeEach(async function () {
    hyperInterface = await hyperInterfaceFactory()
    backend = new BeeInterface(hyperInterface)

    entry = {
      [c.CONTENT]: 'The content',
      [c.CHILDREN]: []
    }

    hyperbee = await hyperInterface.createHyperbee('testbee')
    await hyperbee.put(location, entry)

    beeLocation = new BeeLocation({ hash: hyperbee.feed.key, location })

    node = new Node({ beeLocation, backend })
  })

  this.afterEach(async function () {
    await hyperInterface.close()
  })

  it('Can get content', async function () {
    const content = await node.getContent()
    expect(content).to.equal('The content')
  })

  it('Can get children', async function () {
    const children = await node.getChildren()
    expect(children).to.deep.equal([])
  })

  it('Can create a new node if it doesn\'t already exist', async function () {
    const newLoc = 'newLoc'
    const newBeeLocation = new BeeLocation({ hash: beeLocation.hash, location: newLoc })
    const newNode = new Node({ beeLocation: newBeeLocation, backend })

    expect((await hyperbee.get(newLoc))).to.equal(null)

    await newNode.createIfNotExists()
    const expected = { [c.CONTENT]: {}, [c.CHILDREN]: [] }
    expect((await hyperbee.get(newLoc)).value).to.deep.equal(expected)
  })

  it('Keeps existing node if creating at already-existing location', async function () {
    const newNode = new Node({ beeLocation, backend })
    const original = await hyperbee.get(beeLocation.location)
    expect(original.value.content).to.equal('The content') // Sanity check

    await newNode.createIfNotExists()
    expect((await hyperbee.get(beeLocation.location))).to.deep.equal(original)
  })

  it('Can create a child', async function () {
    const childLoc = 'childLoc'
    const childNode = await node.createChild(childLoc)

    const childEntry = (await hyperbee.get(childLoc)).value
    expect(childEntry).to.deep.equal({ [c.CONTENT]: {}, [c.CHILDREN]: [] })
    expect((await childNode.getEntry())).to.deep.equal(childEntry)

    const nodeChildren = await node.getChildren()
    expect(nodeChildren.length).to.equal(1)
    expect(nodeChildren[0]).to.deep.equal(
      { location: childLoc, version: undefined, hash: node.beeLocation.hash }
    )
  })

  it('Fills in own hash if children have none', async function () {
    const inChildren = [
      { [c.LOCATION]: 'childWithoutHash' },
      { [c.LOCATION]: 'childWithHash', [c.HASH]: 'b'.repeat(64) }
    ]
    entry[c.CHILDREN] = inChildren
    await hyperbee.put(location, entry)

    const outChildren = await node.getChildren()
    const expected = [
      new BeeLocation({ hash: hyperbee.feed.key, location: 'childWithoutHash' }),
      new BeeLocation({ hash: 'b'.repeat(64), location: 'childWithHash' })
    ]
    expect(outChildren.length).to.equal(expected.length).to.equal(2)
    for (let i = 0; i < expected.length; i++) {
      expect({ ...expected[i] }).to.deep.equal({ ...outChildren[i] })
    }
  })

  describe('Test invalid Nodes throw on getEntry', function () {
    const validChild = {
      location: 'some place'
    }

    const invalids = [
      [
        { [c.CONTENT]: 'content', [c.CHILDREN]: 'nope' },
        'children no array'
      ],
      [
        { [c.CONTENT]: 'content', [c.CHILDREN]: ['nope'] },
        'Invalid child in children'
      ],
      [
        { [c.CONTENT]: 'content', [c.CHILDREN]: [validChild, {}] },
        'Only 1 of 2 children valid'
      ],
      [
        { [c.CHILDREN]: [] },
        'No content'
      ],
      [
        { [c.CONTENT]: 'content' },
        'No children'
      ]
    ]

    invalids.forEach(([entry, desc]) => {
      it(`throws on invalid node (${desc})`, async function () {
        const location = 'someLoc'
        await hyperbee.put(location, entry)
        const beeLocation = new BeeLocation(
          { hash: hyperbee.feed.key, location }
        )
        const node = new Node({ beeLocation, backend })

        await nodeAssert.rejects(
          node.getEntry(), { message: /Invalid node entry.*/ }
        )
      })
    })
  })

  describe('Test valid Nodes do not throw on getEntry', function () {
    // DEVNOTE: can't (easily) make use of beforeEach combined with a
    // forEach loop over multiple tests, so we do it the ugly way

    const validChild = {
      location: 'some place'
    }
    const validHashedChild = {
      location: 'some place',
      hash: 'a'.repeat(64)
    }
    const validVersionedChild = {
      location: 'some place',
      hash: 'a'.repeat(64),
      version: 5
    }

    const valids = [
      [
        { [c.CONTENT]: 'content', [c.CHILDREN]: [] },
        'No kids'
      ],
      [
        { [c.CONTENT]: 'content', [c.CHILDREN]: [validChild] },
        'A valid child'
      ],
      [
        {
          [c.CONTENT]: 'content',
          [c.CHILDREN]: [validChild, validHashedChild, validVersionedChild]
        },
        'All valid children'
      ],
      [
        { [c.CONTENT]: null, [c.CHILDREN]: [] },
        'null content'
      ],
      [
        { [c.CONTENT]: 'content', [c.CHILDREN]: [], 'what?': 'okay' },
        'Additional field'
      ]
    ]

    valids.forEach(([entry, desc]) => {
      it(`accepts valid node (${desc})`, async function () {
        const location = 'someLoc'
        await hyperbee.put(location, entry)
        const beeLocation = new BeeLocation(
          { hash: hyperbee.feed.key, location }
        )
        const node = new Node({ beeLocation, backend })

        await nodeAssert.doesNotReject(node.getEntry())
      })
    })
  })

  describe('Can modify a node', function () {
    it("Can modify a node's content", async function () {
      const newContent = 'New content'
      expect((await node.getContent())).to.not.equal(newContent) // Sanity check

      const origBee = await createArrayFromStream(hyperbee.createReadStream())

      await node.setContent(newContent)
      expect((await node.getContent())).to.equal(newContent)

      const newBee = await createArrayFromStream(hyperbee.createReadStream())

      const expected = origBee
      expect(expected.length).to.equal(1)
      expected[0][c.CONTENT] = newContent
      expect(newBee).to.deep.equal(expected)
    })

    it('Can push a child', async function () {
      const origBee = await createArrayFromStream(hyperbee.createReadStream())

      const child1 = { [c.HASH]: 'a'.repeat(64), [c.LOCATION]: 'Loc child1' }
      const child2 = { [c.HASH]: 'a'.repeat(64), [c.LOCATION]: 'Loc child2' }

      await node.pushChild(child1)
      expect((await node.getChildren())[0].toJson()).to.deep.equal(child1)

      await node.pushChild(child2)
      const newBee = await createArrayFromStream(hyperbee.createReadStream())

      const expected = origBee
      expect(expected.length).to.equal(1)
      expected[0][c.CHILDREN] = [child1, child2]
      expect(newBee).to.deep.equal(expected)
    })

    it('Cannot push an invalid child', async function () {
      const child1 = { [c.HASH]: 'a'.repeat(63), [c.LOCATION]: 'Loc child1' }
      await nodeAssert.rejects(
        node.pushChild(child1),
        { message: /Invalid node entry--.*/ }
      )
    })
  })

  describe('Test caching logic', function () {
    it('caches getEntry', async function () {
      const spy = sinon.spy(node.beeLocation, 'resolve')

      expect(spy.callCount).equal(0)
      await node.getEntry()
      expect(spy.callCount).equal(1)
      const cachedEntry = await node.getEntry()
      expect(spy.callCount).equal(1)

      expect(cachedEntry).to.deep.equal(entry)
    })

    it('Invalidates the cache upon setEntry', async function () {
      const spy = sinon.spy(node.beeLocation, 'resolve')

      expect(spy.callCount).equal(0)
      await node.getEntry()
      expect(spy.callCount).equal(1)

      entry[c.CONTENT] = 'New content'
      await node._setEntry(entry)

      const updatedEntry = await node.getEntry()
      expect(spy.callCount).equal(2)
      expect(updatedEntry[c.CONTENT]).to.equal('New content')
    })
  })
})

async function createArrayFromStream (stream) {
  const content = []
  for await (const { value } of stream) {
    content.push(value)
  }
  return content
}
