const { expect } = require('chai')

const { hyperInterfaceFactory } = require('./fixtures')
const c = require('../lib/constants')
const BeeInterface = require('../lib/bee-interface')

describe('Test bee-interface', function () {
  const location = 'loc'
  let hyperInterface, bee, beeInterface
  let beeLocation, entry

  this.beforeEach(async function () {
    hyperInterface = await hyperInterfaceFactory()
    beeInterface = new BeeInterface(hyperInterface)

    entry = {
      [c.CONTENT]: 'The content',
      [c.CHILDREN]: []
    }

    bee = await hyperInterface.createHyperbee('testbee')
    await bee.put(location, entry)

    beeLocation = { hash: bee.feed.key, location }
  })

  this.afterEach(async function () {
    await hyperInterface.close()
  })

  it('has a valid getEntry function', async function () {
    const gottenEntry = await beeInterface.getEntry(beeLocation)
    expect(gottenEntry).to.deep.equal(entry)
  })

  it('has a valid setEntry function', async function () {
    const newEntry = { ...entry, [c.CONTENT]: 'New content' }
    await beeInterface.setEntry({ key: bee.feed.key, location, entry: newEntry })

    expect((await beeInterface.getEntry(beeLocation))).to.deep.equal({
      [c.CONTENT]: 'New content',
      [c.CHILDREN]: []
    })
  })
})
