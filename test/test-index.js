const { expect } = require('chai')
const { strict: nodeAssert } = require('assert')

const utils = require('../lib/utils')
const { hyperInterfaceFactory } = require('./fixtures')
const c = require('../lib/constants')
const BeeLocation = require('../lib/bee-location')
const { DiGraph } = require('..')
const BeeInterface = require('../lib/bee-interface')
const Node = require('../lib/node')

describe('Test DiGraph', function () {
  const locRoot = c.ROOT
  const locEntry1 = 'locEntry1'
  const locEntry2 = 'locEntry2'
  const contRoot = 'Content root'
  const contEntry1 = 'Content entry1'
  const contEntry2 = 'Content entry2'
  const netWorkDelayMs = 100

  let hyperInterface, bee
  let rootBeeLocation
  let entry1, entry2, rootEntry
  let delayedBackend, backend
  let diGraph
  let child1, child2

  this.beforeEach(async function () {
    hyperInterface = await hyperInterfaceFactory()
    backend = new BeeInterface(hyperInterface)

    const delayedGetEntryFunc = async (entry) => {
      // Simulates peer-finding + network delay
      await new Promise(resolve => setTimeout(resolve, netWorkDelayMs))
      return await hyperInterface.getEntry(entry)
    }
    delayedBackend = new BeeInterface(hyperInterface)
    delayedBackend.getEntry = delayedGetEntryFunc

    child1 = { [c.LOCATION]: locEntry1 }
    child2 = { [c.LOCATION]: locEntry2 }
    entry1 = {
      [c.CONTENT]: contEntry1,
      [c.CHILDREN]: []
    }

    entry2 = {
      [c.CONTENT]: contEntry2,
      [c.CHILDREN]: []
    }

    rootEntry = {
      [c.CONTENT]: contRoot,
      [c.CHILDREN]: [child1, child2]
    }

    bee = await hyperInterface.createHyperbee('testbee')
    await bee.put(locEntry1, entry1)
    await bee.put(locEntry2, entry2)
    await bee.put(locRoot, rootEntry)

    rootBeeLocation = new BeeLocation(
      { hash: bee.feed.key, location: locRoot }
    )

    diGraph = new DiGraph({ rootBeeLocation, backend })
  })

  this.afterEach(async function () {
    await hyperInterface.close()
  })

  it('Allows passing custom node class', async function () {
    // Sanity check
    const allNodes = await _getAllNodes(diGraph)
    expect(allNodes[0].extraFunc).to.equal(undefined)

    diGraph = new DiGraph({ rootBeeLocation, backend, NodeClass: CustomNode })
    const allCustomNodes = await _getAllNodes(diGraph)
    expect(allCustomNodes[0].extraFunc()).to.equal('I am custom node')
  })

  it('Can create a new digraph with createNew', async function () {
    diGraph = await DiGraph.createNew({ name: 'my graph', hyperInterface })
    const allNodes = await _getAllNodes(diGraph)

    expect(allNodes.length).to.equal(1)
    expect((await allNodes[0].getContent())).to.deep.equal({})
    expect((await allNodes[0].getChildren())).to.deep.equal([])
  })

  it('createNew passes options to constructor', async function () {
    diGraph = await DiGraph.createNew(
      { name: 'my graph', hyperInterface, NodeClass: CustomNode }
    )
    const allNodes = await _getAllNodes(diGraph)

    expect((await allNodes[0].extraFunc())).to.equal('I am custom node')
  })

  it('Can load a digraph from a bee', async function () {
    diGraph = await DiGraph.loadFromBee({ hash: bee.feed.key, hyperInterface })
    const allNodes = await _getAllNodes(diGraph)

    expect(allNodes.length).to.equal(3)
    expect((await allNodes[0].getEntry())).to.deep.equal(rootEntry)
  })

  it('passes options to constructor for loadFromBee', async function () {
    diGraph = await DiGraph.loadFromBee(
      { hash: bee.feed.key, hyperInterface, NodeClass: CustomNode }
    )
    const allNodes = await _getAllNodes(diGraph)

    expect((await allNodes[0].extraFunc())).to.equal('I am custom node')
  })

  it('Throws when loading a digraph from bee without standard root location', async function () {
    await bee.del(locRoot)
    await bee.put('otherLoc', rootEntry)

    await nodeAssert.rejects(
      DiGraph.loadFromBee({ hash: bee.feed.key, hyperInterface }),
      { message: /No entry at 'root'.+/ }
    )
  })

  it('Can get the root hash of a digraph', async function () {
    expect(diGraph.rootHash).to.deep.equal(utils.getHashAsStr(bee.feed.key))
  })

  it('Can yield all nodes once', async function () {
    const allNodes = await _getAllNodes(diGraph)

    const expectedContents = new Set([contRoot, contEntry1, contEntry2])
    expect(allNodes.length).to.equal(expectedContents.size)

    const actualContents = await _getNodeContentSet(allNodes)
    expect(expectedContents).to.deep.equal(actualContents)
  })

  it('Can yield all nodes once with repeated entries', async function () {
    entry2[c.CHILDREN].push(child1)
    await bee.put(locEntry2, entry2)

    const allNodes = await _getAllNodes(diGraph)

    const expectedContents = new Set([contRoot, contEntry1, contEntry2])
    expect(allNodes.length).to.equal(expectedContents.size)

    const actualContents = await _getNodeContentSet(allNodes)
    expect(expectedContents).to.deep.equal(actualContents)
  })

  it('walks steps as expected', async function () {
    const step1 = await diGraph.recStepper()
    expect((await step1.node.getContent())).to.equal(rootEntry[c.CONTENT])
    expect(step1.childYielders.length).to.equal(2)

    const step2a = await step1.childYielders[0]()
    expect((await step2a.node.getContent())).to.equal(entry1[c.CONTENT])
    expect(step2a.childYielders.length).to.equal(0)

    const step2b = await step1.childYielders[1]()
    expect((await step2b.node.getContent())).to.equal(entry2[c.CONTENT])
    expect(step2b.childYielders.length).to.equal(0)
  })

  it('Walks depth first correctly', async function () {
    const content3 = 'Content3'
    const content4 = 'Content4'

    const node1BeeLocation = (await diGraph.rootNode.getChildren())[0]
    const node1 = new Node({ beeLocation: node1BeeLocation, backend: diGraph.backend })

    const node3 = await node1.createChild('location3')
    await node3.setContent(content3)

    const node4 = await node1.createChild('location4')
    await node4.setContent(content4)
    await node4.createChild('location3')
    await node4.createChild('location3')

    const contents = []
    for await (const node of diGraph.walkDepthFirst()) {
      contents.push((await node.getContent()))
    }
    expect(contents).to.deep.equal([
      contRoot, contEntry1, content3, content4, content3, content3, contEntry2
    ])
  })

  it('visits the same node multiple times when walking depth first', async function () {
    entry2[c.CHILDREN].push(child1)
    await bee.put(locEntry2, entry2)

    const contents = []
    for await (const node of diGraph.walkDepthFirst()) {
      contents.push((await node.getContent()))
    }
    expect(contents.length).to.equal(4)
    expect((contents.filter((content) => content === entry1[c.CONTENT])).length).to.equal(2)
  })

  it('follows loops when walking depth first', async function () {
    entry2[c.CHILDREN].push(child1)
    await bee.put(locEntry2, entry2)

    entry1[c.CHILDREN].push(child2)
    await bee.put(locEntry1, entry1)

    const nodes = []
    const maxVisits = 20
    let nrVisits = 0
    for await (const node of diGraph.walkDepthFirst()) {
      nodes.push(node)
      nrVisits += 1
      if (nrVisits >= maxVisits) {
        break
      }
    }

    const contents = await getNodeContentList(nodes)
    expect(contents.length).to.equal(maxVisits)
    // Loop of size 2, so every second entry is equal
    expect(
      contents[maxVisits - 1]).to.not.equal(contents[maxVisits - 2]
    ) // Sanity check
    expect(
      (new Set([contents[maxVisits - 1], contents[maxVisits - 3], contents[maxVisits - 5]])).size
    ).to.equal(1)
    expect(
      (new Set([contents[maxVisits - 2], contents[maxVisits - 4], contents[maxVisits - 6]])).size
    ).to.equal(1)
  })

  it('Simultaneously awaits all children when walking all nodes once', async function () {
    const entryGenerator = entryGeneratorFunc(3, bee)

    for (let i = 3; i < 10; i++) {
      const entry = (await entryGenerator.next()).value
      rootEntry[c.CHILDREN].push({ [c.LOCATION]: entry[c.LOCATION] })
    }
    await bee.put(locRoot, rootEntry)

    diGraph = new DiGraph({ rootBeeLocation, backend: delayedBackend })

    const startTime = new Date()
    const allNodes = await _getAllNodes(diGraph)
    const timeMs = new Date() - startTime

    // non-simultaneous exec would be > 10*100ms
    // Underbound here is 2*100 (root->entry-x)
    expect(timeMs).to.be.lessThan(500)
    expect(allNodes.length).to.equal(10) // Sanity check
  })

  it('Simultaneous awaits when walking all nodes once -- complex scenario', async function () {
    const entryGenerator = entryGeneratorFunc(3, bee)

    const entry1Child1 = (await entryGenerator.next()).value
    const entry1Child2 = (await entryGenerator.next()).value
    const entry1Child3 = (await entryGenerator.next()).value
    entry1[c.CHILDREN] = [entry1Child1, entry1Child2, entry1Child3]
    await bee.put(locEntry1, entry1)

    const entry2Child1 = (await entryGenerator.next()).value
    const entry2Child2 = (await entryGenerator.next()).value
    const entry2Child3 = (await entryGenerator.next()).value
    entry2[c.CHILDREN] = [entry2Child1, entry2Child2, entry2Child3]
    await bee.put(locEntry2, entry2)

    const lvl3Child1 = (await entryGenerator.next()).value
    const lvl3Child2 = (await entryGenerator.next()).value
    const lastEntryLoc = entry2Child3[c.LOCATION]
    const { value: lastEntry } = await bee.get(lastEntryLoc)
    lastEntry[c.CHILDREN] = [lvl3Child1, lvl3Child2]
    await bee.put(lastEntryLoc, lastEntry)

    diGraph = new DiGraph({ rootBeeLocation, backend: delayedBackend })

    const startTime = new Date()
    const allNodes = await _getAllNodes(diGraph)
    const timeMs = new Date() - startTime

    // non-simultaneous exec would be > 11*100ms
    // Best possible here is 4*100ms (root->entry2->entry2child3->lvl3Child2)
    expect(timeMs).to.be.lessThan(700) // Some margin
    expect(allNodes.length).to.equal(11) // Sanity check
  })

  it('Deep validates correct digraph', async function () {
    await nodeAssert.doesNotReject(diGraph.deepValidate())
  })

  describe('Throws when deep-validating incorrect digraph', async function () {
    it('Throws on invalid child node', async function () {
      delete entry2[c.CONTENT]
      await bee.put(locEntry2, entry2)

      await nodeAssert.rejects(
        diGraph.deepValidate(), { message: /Invalid node entry.+property 'content'.+/ }
      )
    })

    it('Throws on child not found', async function () {
      await bee.del(locEntry1)

      await nodeAssert.rejects(
        diGraph.deepValidate(), { message: /No entry at 'locEntry1' for.+/ }
      )
    })
  })

  it('walkDepthFirst starts resolving siblings simultaneously', async function () {
    const entryGenerator = entryGeneratorFunc(3, bee)

    for (let i = 3; i < 10; i++) {
      const entry = (await entryGenerator.next()).value
      rootEntry[c.CHILDREN].push({ [c.LOCATION]: entry[c.LOCATION] })
    }
    await bee.put(locRoot, rootEntry)

    diGraph = new DiGraph({ rootBeeLocation, backend: delayedBackend })

    const allNodes = []
    const startTime = new Date()
    for await (const node of diGraph.walkDepthFirst()) {
      allNodes.push(node)
    }
    const timeMs = new Date() - startTime

    expect(allNodes.length).to.equal(10)
    // 1 delay for root, simultaneous delays for all other nodes
    expect(timeMs).to.be.lessThan(netWorkDelayMs * 4)
  })

  it('Throws if unable to get child while walking', async function () {
    // TODO: this should not throw, but stay pending
    // after having sent out an event requesting that bee
    // (easiest with a hyperbee watch interface)
    // --this tests the current expected behaviour
    await bee.del(locEntry1)
    await nodeAssert.rejects(
      _getAllNodes(diGraph),
      { message: /No entry at 'locEntry1'.+/ }
    )
  })

  describe('Test indexing logic', function () {
    it('Can create an index if no other bees referenced', async function () {
      await diGraph.createIndex()
      const { value: root } = await bee.get(c.ROOT)
      expect(root[c.INDEX][c.REFERENCED_BEES]).to.deep.equal([])
    })

    it('Can get the referenced bees', async function () {
      rootEntry[c.INDEX] = { [c.REFERENCED_BEES]: ['a'.repeat(64)] }
      await bee.put(c.ROOT, rootEntry)
      expect((await diGraph.getReferencedBees())).to.deep.equal(['a'.repeat(64)])
    })

    it('Returns empty list if referenced bees not present', async function () {
      expect((await diGraph.getReferencedBees())).to.deep.equal([])

      rootEntry[c.INDEX] = {}
      await bee.put(rootEntry)
      expect((await diGraph.getReferencedBees())).to.deep.equal([])
    })

    it('Does not delete other index-entries', async function () {
      rootEntry[c.INDEX] = { something: 'remains' }
      await bee.put(c.ROOT, rootEntry)
      await diGraph.createIndex()
      const index = (await bee.get(c.ROOT)).value.index
      expect(index).to.deep.equal(
        { something: 'remains', [c.REFERENCED_BEES]: [] }
      )
    })

    describe('Test indexing logic with referenced bees', async function () {
      let refNode1, refNode2
      let refNode1Entry, refNode2Entry
      let bee2, bee3
      let bee2Hash, bee3Hash

      this.beforeEach(async function () {
        bee2 = await hyperInterface.createHyperbee('bee2')
        bee3 = await hyperInterface.createHyperbee('bee3')
        bee2Hash = utils.getHashAsStr(bee2.feed.key)
        bee3Hash = utils.getHashAsStr(bee3.feed.key)

        refNode1Entry = new BeeLocation({ hash: bee2Hash, location: 'onlyEntry' })
        refNode2Entry = new BeeLocation({ hash: bee3Hash, location: 'onlyEntry' })
        refNode1 = new Node({ beeLocation: refNode1Entry, backend })
        await refNode1.createIfNotExists()
        refNode2 = new Node({ beeLocation: refNode2Entry, backend })
        await refNode2.createIfNotExists()

        entry1[c.CHILDREN] = [refNode1Entry, refNode2Entry]
        await bee.put(locEntry1, entry1)

        await diGraph.createIndex()
      })

      it('Gets the correct referencedBees', async function () {
        const expected = new Set([bee2Hash, bee3Hash])
        expect(new Set(await diGraph.getReferencedBees())).to.deep.equal(expected)
      })
    })
  })
})

async function _getAllNodes (diGraph) {
  const allNodes = []
  for await (const node of diGraph.yieldAllNodesOnce()) {
    allNodes.push(node)
  }
  return allNodes
}

async function _getNodeContentSet (nodes) {
  return new Set(await getNodeContentList(nodes))
}

async function getNodeContentList (nodes) {
  return await Promise.all(
    nodes.map(async (node) => await node.getContent())
  )
}

async function * entryGeneratorFunc (startI, bee) {
  let entryNr = startI
  while (true) {
    const entry = {
      [c.CONTENT]: `Content ${entryNr}`,
      [c.CHILDREN]: []
    }
    const loc = `entry ${entryNr}`
    await bee.put(loc, entry)
    yield { [c.LOCATION]: loc }
    entryNr++
  }
}

class CustomNode extends Node {
  extraFunc () {
    return 'I am custom node'
  }
}
