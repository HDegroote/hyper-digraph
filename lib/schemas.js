const c = require('./constants')

function getBeeLocationSchema () {
  return {
    type: 'object',
    properties: {
      [c.HASH]: {
        description: 'Hypercore hash in hexadecimal',
        type: 'string',
        pattern: c.HYPERHASH_REGEX_STR
      },
      [c.LOCATION]: {
        description: 'Identifying key in the hyperbee',
        type: 'string',
        minLength: 1
      },
      [c.VERSION]: {
        description: 'Hyperbee version (assumes latest if not present)',
        type: 'integer'
      }
    },
    required: [c.HASH, c.LOCATION]
  }
}

function getNodeSchema () {
  const childSchema = getBeeLocationSchema()
  const requireds = childSchema.required.filter(
    (entry) => entry !== c.HASH // hash optional for child entries
  )
  childSchema.required = requireds
  childSchema.properties[c.HASH].description += ' (if not present, assumes parent hash)'

  return {
    properties: {
      [c.CONTENT]: {
        description: 'The content of the node (any type)'
      },
      [c.CHILDREN]: {
        description: 'Children of the node',
        type: 'array',
        items: childSchema
      }
    },
    type: 'object',
    required: [c.CONTENT, c.CHILDREN]
  }
}

module.exports = {
  getBeeLocationSchema,
  getNodeSchema
}
