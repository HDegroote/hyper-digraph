module.exports = {
  CONTENT: 'content',
  CHILDREN: 'children',
  HYPERHASH_REGEX_STR: '^[0-9a-fA-F]{64,64}$',
  HASH: 'hash',
  LOCATION: 'location',
  VERSION: 'version',
  ROOT: 'root',
  INDEX: 'index',
  REFERENCED_BEES: 'referencedBees'
}
