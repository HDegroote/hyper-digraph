const b4a = require('b4a')

function getHashAsStr (key) {
  const res = typeof key === 'string' ? key : b4a.toString(key, 'hex')
  return res
}

function ajvErrorToStr (error, jsonObj = undefined) {
  let res = `${error.instancePath}: ${error.message}`

  if (jsonObj !== undefined) {
    res += ` (${JSON.stringify(jsonObj, null, 1)})`
  }

  return res
}

module.exports = {
  getHashAsStr,
  ajvErrorToStr
}
