const Ajv = require('ajv')

const utils = require('./utils')
const { getBeeLocationSchema } = require('./schemas')
const c = require('./constants')

const ajv = new Ajv()
const validate = ajv.compile(getBeeLocationSchema())

class BeeLocation {
  constructor ({ hash, location, version = undefined }) {
    this.hash = hash != null ? utils.getHashAsStr(hash) : hash // will fail later
    this.location = location
    this.version = version

    this.assertIsValid()
  }

  assertIsValid () {
    const asDict = { ...this }
    if (!validate(asDict)) {
      throw new Error(utils.ajvErrorToStr(validate.errors[0], asDict))
    }
  }

  get uniqueId () {
    return this.hash + this.location + this.version
  }

  async resolve (getEntryFunc) {
    return await getEntryFunc(this.toJson())
  }

  toJson () {
    const res = {
      [c.LOCATION]: this.location,
      [c.HASH]: this.hash
    }
    if (this.version != null) {
      res[c.VERSION] = this.version
    }

    return res
  }

  toStr () {
    let res = `${this.hash}/${this.location}`
    res += this.version === undefined ? '[latest]' : `[${this.version}]`
    return res
  }
}

module.exports = BeeLocation
