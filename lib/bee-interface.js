class BeeInterface {
  constructor (hyperInterface) {
    this.hyperInterface = hyperInterface

    this.getEntry = async (beeLocation) => {
      return await this.hyperInterface.getEntry(beeLocation)
    }

    this.setEntry = async ({ key, location, entry, onlyIfNew = false }) => {
      const bee = await this.hyperInterface.readHyperbee(key)

      if (onlyIfNew) {
        // Cas is considered only if there already exists an entry, so
        // by always returning false, it will never modify an existing entry
        await bee.put(location, entry, { cas: () => false })
      } else {
        await bee.put(location, entry)
      }
    }
  }
}

module.exports = BeeInterface
