const c = require('./constants')
const BeeLocation = require('./bee-location')
const Ajv = require('ajv')
const { getNodeSchema } = require('./schemas')
const utils = require('./utils')

const ajv = new Ajv()
const validate = ajv.compile(getNodeSchema())

class Node {
  #resolvedEntry
  constructor ({ beeLocation, backend }) {
    if (backend == null) {
      throw new Error('A node requires a backend')
    }

    this.beeLocation = new BeeLocation({ ...beeLocation })
    this.backend = backend
    this.#resolvedEntry = null
  }

  async createIfNotExists () {
    await this._setEntry(this.constructor.getEmptyNode(), { onlyIfNew: true })
  }

  async createChild (location) {
    const childbeeLocation = new BeeLocation(
      { hash: this.beeLocation.hash, location }
    )
    const child = new Node({ beeLocation: childbeeLocation, backend: this.backend })
    await child.createIfNotExists()

    await this.pushChild({ location })

    return child
  }

  async getEntry () {
    if (this.#resolvedEntry === null) {
      // DEVNOTE: to keep it simple, we don't use a lock
      // so other getEntry calls can start awaiting before the first resolves,
      // and the last to resolve will decide on the final value of resolvedEntry
      const resolvedEntry = await this.beeLocation.resolve(this.backend.getEntry)
      this.validateEntry(resolvedEntry)
      this.#resolvedEntry = resolvedEntry
    }
    return JSON.parse(JSON.stringify(this.#resolvedEntry))
  }

  async _setEntry (entry, { onlyIfNew = false } = {}) {
    this.validateEntry(entry)
    await this.backend.setEntry({
      key: this.beeLocation.hash,
      location: this.beeLocation.location,
      entry,
      onlyIfNew
    })
    this.invalidateCache()
  }

  invalidateCache () {
    this.#resolvedEntry = null
  }

  validateEntry (entry) {
    if (!validate(entry)) {
      Node.throwFromValidationError(validate, entry, this.beeLocation)
    }
  }

  static throwFromValidationError (validate, entry, beeLocation) {
    const errorMsg = utils.ajvErrorToStr(validate.errors[0], entry)
    throw new Error(
      `Invalid node entry--${errorMsg} (${beeLocation.toStr()})`
    )
  }

  async getContent () {
    return (await this.getEntry())[c.CONTENT]
  }

  async setContent (newContent) {
    const entry = await this.getEntry()

    entry[c.CONTENT] = newContent
    await this._setEntry(entry)
  }

  async getChildren () {
    const rawChildren = (await this.getEntry())[c.CHILDREN]

    const children = rawChildren.map((child) => {
      // Use own hash if child has no explicit hash
      child.hash = child.hash ?? this.beeLocation.hash
      return new BeeLocation({ ...child })
    })

    return children
  }

  async pushChild ({ location, hash = undefined, version = undefined }) {
    hash = hash !== undefined ? utils.getHashAsStr(hash) : hash
    const entry = await this.getEntry()

    entry[c.CHILDREN].push({ location, hash, version })
    await this._setEntry(entry)
  }

  static getEmptyNode () {
    return {
      [c.CONTENT]: {},
      [c.CHILDREN]: []
    }
  }
}

module.exports = Node
