const HyperInterface = require('hyperpubee-hyper-interface')
const { DiGraph } = require('.') // or if not cloned: require('hyper-digraph')
const ram = require('random-access-memory') // Might need to be installed
const Corestore = require('corestore')

const corestore = new Corestore(ram)
const hyperInterface = new HyperInterface(corestore)

async function runExample () {
  // Creating a new DiGraph with only an empty root node
  const diGraph = await DiGraph.createNew({
    name: 'My digraph',
    hyperInterface
  })

  // Set the content of a node
  await diGraph.rootNode.setContent('I am the root node')

  // A DiGraph can also be loaded from a compatible hyperbee
  const bee = await hyperInterface.createHyperbee('My other digraph')
  await bee.put(
    'root',
    { content: 'I am the root of the other digraph', children: [{ location: 'aChild' }] }
  )
  await bee.put(
    'aChild',
    { content: 'I am a child of the other digraph', children: [] }
  )
  const otherDiGraph = await DiGraph.loadFromBee(
    { hash: bee.feed.key, hyperInterface }
  )

  // Checks that the bee is indeed a valid digraph
  await otherDiGraph.deepValidate()

  console.log(await otherDiGraph.rootNode.getChildren()) // 1 entry at location 'aChild'

  // Add a new local node as child
  const localChild = await diGraph.rootNode.createChild('localChild')
  await localChild.setContent('I am a local child')

  // Add a new remote node to a child
  // (Note: the digraph is extended with this node and all its children)
  await diGraph.rootNode.pushChild({ hash: bee.feed.key, location: 'root' })

  // Create an index, containing which bees are referenced
  await diGraph.createIndex()

  // You should ensure that you have read access to all referenced bees
  // before accessing a digraph's nodes (e.g. if you don't have them locally:
  // connect with a peer who does, using hyperswarm for example)
  const referencedBees = await diGraph.getReferencedBees()
  console.log('The digraph references these bees: ', referencedBees)

  console.log('\nEfficiently yield all nodes from the di-graph (note: no order guarantees)')
  for await (const node of diGraph.yieldAllNodesOnce()) {
    console.log(node.beeLocation.toStr(), ': ', (await node.getContent()))
  }
  // Prints something like
  /*
    8212ab4da51b3b8844911a069a6195cee3fe31c6ac1de7106b0af2ccebcc4a44/root[latest] :  I am the root node
    8212ab4da51b3b8844911a069a6195cee3fe31c6ac1de7106b0af2ccebcc4a44/localChild[latest] :  I am a local child
    32bb6abe15f6fd3e3a7fd637b8592ebb161ee8891a9d679923a8dfcff0e0e3f3/root[latest] :  I am the root of the other digraph
    32bb6abe15f6fd3e3a7fd637b8592ebb161ee8891a9d679923a8dfcff0e0e3f3/aChild[latest] :  I am a child of the other digraph
  */

  console.log('\nWalk the di-graph depth-first')
  for await (const node of diGraph.walkDepthFirst()) {
    console.log(node.beeLocation.toStr(), ': ', (await node.getContent()))
  }
}

runExample()
