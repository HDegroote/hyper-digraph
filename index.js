const BeeLocation = require('./lib/bee-location')
const Node = require('./lib/node')
const c = require('./lib/constants')
const BeeInterface = require('./lib/bee-interface')

class DiGraph {
  constructor ({ rootBeeLocation, backend, NodeClass = Node }) {
    this.rootBeeLocation = new BeeLocation({ ...rootBeeLocation })
    this.backend = backend
    this.NodeClass = NodeClass
  }

  get rootHash () {
    return this.rootBeeLocation.hash
  }

  get rootNode () {
    return new this.NodeClass({ beeLocation: this.rootBeeLocation, backend: this.backend })
  }

  async getReferencedBees () {
    const res = (await this.rootNode.getEntry())[c.INDEX]?.[c.REFERENCED_BEES]
    return res != null ? res : []
  }

  async deepValidate () {
    const nodeEntryPromises = []
    for await (const node of this.yieldAllNodesOnce()) {
      nodeEntryPromises.push(node.getEntry())
    }

    // To check the leaf-nodes have valid entries
    await Promise.all(nodeEntryPromises)
  }

  async * walkDepthFirst () {
    // Note: can be infinite generator if the digraph contains a loop
    const stepper = await this.recStepper()
    yield * this._yieldFromStepperDepthFirst(stepper)
  }

  async * _yieldFromStepperDepthFirst ({ node, childYielders }) {
    yield node

    // Already start resolving all children
    const childPromises = childYielders.map((yieldChild) => yieldChild())

    for (const childPromise of childPromises) {
      const nextStep = await childPromise
      yield * this._yieldFromStepperDepthFirst(nextStep)
    }
  }

  async recStepper (beeLocation = undefined) {
    beeLocation = beeLocation ?? this.rootBeeLocation

    const node = new this.NodeClass({ beeLocation, backend: this.backend })

    const childYielders = []
    for (const child of (await node.getChildren())) {
      const yieldChild = async () => await this.recStepper(child)
      childYielders.push(yieldChild)
    }

    return { node, childYielders }
  }

  async * yieldAllNodesOnce () {
    // Walks the di-graph starting from the root, and yields
    // each node once, without guaranteeing any order.
    // If a node is reachable from multiple paths,
    // all paths will reach that node, but only the first
    // one to reach it will yield it and continue with its children
    // --children are resolved 'simultaneously' in async sense
    // (the algorithm does not descend child per child, but requests all
    // and yields nodes in the order in which they are resolved)
    yield * this._yieldAllNodesOnceRec(
      { visitedEntries: new Set(), beeLocation: this.rootBeeLocation }
    )
  }

  async * _yieldAllNodesOnceRec ({ visitedEntries, beeLocation }) {
    // DEVNOTE: all recursive invocations share
    // the same visitedEntries set. There must not be any
    // await between the check on whether it contains an entry,
    // and adding that entry to it. Otherwise entries might be visited
    // multiple times.
    if (visitedEntries.has(beeLocation.uniqueId)) return
    visitedEntries.add(beeLocation.uniqueId)

    const node = new this.NodeClass({ beeLocation, backend: this.backend })
    yield node

    const childGenerators = []
    const allPromises = {}
    for (const child of (await node.getChildren())) {
      const childGenerator = this._yieldAllNodesOnceRec({ visitedEntries, beeLocation: child })
      childGenerators.push(childGenerator)

      // DEVNOTE: i is wrapped in promise as well, to know upon resolution
      // what the corresponding childGenerator index was
      const i = childGenerators.length - 1
      allPromises[i] = Promise.all([i, childGenerator.next()])
    }

    while (Object.keys(allPromises).length > 0) {
      const [winnerIndex, winner] = await Promise.race(Object.values(allPromises))

      if (winner.done === true) {
        delete allPromises[winnerIndex]
      } else {
        allPromises[winnerIndex] = Promise.all(
          [winnerIndex, childGenerators[winnerIndex].next()]
        )
        yield winner.value
      }
    }
  }

  async createIndex () {
    const rootEntryPromise = this.rootNode.getEntry()

    const hashes = new Set()
    for await (const node of this.yieldAllNodesOnce()) {
      hashes.add(node.beeLocation.hash)
    }
    hashes.delete(this.rootNode.beeLocation.hash) // No need to store ourselves

    const rootEntry = await rootEntryPromise
    rootEntry[c.INDEX] ??= {}

    rootEntry[c.INDEX][c.REFERENCED_BEES] = Array.from(hashes)
    await this.rootNode._setEntry(rootEntry)
  }

  static async createNew ({ name, hyperInterface, ...opts }) {
    const bee = await hyperInterface.createHyperbee(name)
    await bee.put(c.ROOT, Node.getEmptyNode())

    const rootBeeLocation = _getRootBeeLocation(bee.feed.key)
    const backend = new BeeInterface(hyperInterface)

    return new DiGraph({ backend, rootBeeLocation, ...opts })
  }

  static async loadFromBee ({ hash, hyperInterface, ...opts }) {
    const backend = new BeeInterface(hyperInterface)

    const rootBeeLocation = _getRootBeeLocation(hash)
    await rootBeeLocation.resolve(backend.getEntry) // Ensures the root is valid

    return new DiGraph({ backend, rootBeeLocation, ...opts })
  }
}

function _getRootBeeLocation (hash) {
  return new BeeLocation(
    { hash, location: c.ROOT }
  )
}

module.exports = { DiGraph, Node, BeeInterface, BeeLocation }
